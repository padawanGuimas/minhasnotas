import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController, NavParams, ToastController } from '@ionic/angular';
import { DisciplinaService } from './../../services/disciplina.service';

@Component({
  selector: 'app-disciplina',
  templateUrl: './disciplina.page.html',
  styleUrls: ['./disciplina.page.scss']

})
export class DisciplinaPage {
  title: string;
  formulario: FormGroup;
  disciplina: any;
  testando: any;

  constructor(private fb: FormBuilder, public navControlador: NavController, private mensagemControler: ToastController,
    public navParams: NavParams, private service: DisciplinaService, public router: ActivatedRoute) {
    this.disciplina = {};
    
    this.router.queryParams.subscribe(
      params => {
        if (params && params.disciplina) {
          this.disciplina = JSON.parse(params.disciplina);
        }else{
          this.disciplina = {};
        }
        this.setupPage();
      }
    );
    
  }

  private setupPage() {
    this.title = this.disciplina.key ? 'Alterando Disciplina' : 'Nova disciplina';
    this.createForm();
  }

  createForm() {
    this.formulario = this.fb.group({
      key: [this.disciplina.key],
      nome: [this.disciplina.nome, Validators.required],
      ano: [this.disciplina.ano, Validators.required],
      observacao: [this.disciplina.observacao],
    });
  }
  onSubmit() {
    if (this.formulario.valid) {
      this.service.save(this.formulario.value)
        .then(() => {
          this.mensagemControler.create({ message: 'Disciplina salvo com sucesso.', duration: 3000 }).finally();
          this.navControlador.pop();
          this.createForm();
        })
        .catch((e) => {
          this.mensagemControler.create({ message: 'Erro ao salvar a disciplina.', duration: 3000 }).finally();
          console.error(e);
        });
    }
  }

  retornarMenu(){
    this.navControlador.navigateRoot('list-disciplina');
  }

}
