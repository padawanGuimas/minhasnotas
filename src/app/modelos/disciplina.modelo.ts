export interface Disciplina {
  key: string;
  nome: string;
  ano: string;
  observacao: string;
};